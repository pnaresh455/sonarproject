package com.sonar.SonarProject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SonarProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(SonarProjectApplication.class, args);
	}

}
